﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MyMSACS.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Degree",
                columns: table => new
                {
                    DegreeId = table.Column<int>(type: "int", nullable: false),
                    DegreeAbbrev = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    DegreeName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Degree", x => x.DegreeId);
                });

            migrationBuilder.CreateTable(
                name: "DegreeRequirement",
                columns: table => new
                {
                    DegreeRequirementId = table.Column<int>(type: "int", nullable: false),
                    DegreeId = table.Column<int>(type: "int", nullable: false),
                    RequirementAbbrev = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    RequirementName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    RequirementNumber = table.Column<int>(type: "int", nullable: false),
                    lkDegree = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DegreeRequirement", x => x.DegreeRequirementId);
                });

            migrationBuilder.CreateTable(
                name: "DegreeStatus",
                columns: table => new
                {
                    DegreeStatusId = table.Column<int>(type: "int", nullable: false),
                    DegreeState = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DegreeStatus", x => x.DegreeStatusId);
                });

            migrationBuilder.CreateTable(
                name: "PlanTerm",
                columns: table => new
                {
                    PlanTermId = table.Column<int>(type: "int", nullable: false),
                    DegreeId = table.Column<int>(type: "int", nullable: false),
                    PlanNumber = table.Column<int>(type: "int", nullable: false),
                    StudentId = table.Column<int>(type: "int", nullable: false),
                    TermAbbrev = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    TermNumber = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanTerm", x => x.PlanTermId);
                });

            migrationBuilder.CreateTable(
                name: "PlanTermRequirement",
                columns: table => new
                {
                    PlanTermRequirementId = table.Column<int>(type: "int", nullable: false),
                    DegreeId = table.Column<int>(type: "int", nullable: false),
                    PlanNumber = table.Column<int>(type: "int", nullable: false),
                    RequirementNumber = table.Column<int>(type: "int", nullable: false),
                    StudentId = table.Column<int>(type: "int", nullable: false),
                    TermNumber = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanTermRequirement", x => x.PlanTermRequirementId);
                });

            migrationBuilder.CreateTable(
                name: "RequirementStatus",
                columns: table => new
                {
                    RequirementStatusId = table.Column<int>(type: "int", nullable: false),
                    RequirementState = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequirementStatus", x => x.RequirementStatusId);
                });

            migrationBuilder.CreateTable(
                name: "Student",
                columns: table => new
                {
                    StudentId = table.Column<int>(type: "int", nullable: false),
                    FamilyName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    GivenName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Student", x => x.StudentId);
                });

            migrationBuilder.CreateTable(
                name: "StudentDegreePlan",
                columns: table => new
                {
                    StudentDegreePlanId = table.Column<int>(type: "int", nullable: false),
                    CreateDate = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    DegreeId = table.Column<int>(type: "int", nullable: false),
                    DegreeStatusId = table.Column<int>(type: "int", nullable: false),
                    EditDate = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    PlanAbbrev = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    PlanName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    PlanNumber = table.Column<int>(type: "int", nullable: false),
                    StudentId = table.Column<int>(type: "int", nullable: false),
                    lkDegreeStatus = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentDegreePlan", x => x.StudentDegreePlanId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Degree");

            migrationBuilder.DropTable(
                name: "DegreeRequirement");

            migrationBuilder.DropTable(
                name: "DegreeStatus");

            migrationBuilder.DropTable(
                name: "PlanTerm");

            migrationBuilder.DropTable(
                name: "PlanTermRequirement");

            migrationBuilder.DropTable(
                name: "RequirementStatus");

            migrationBuilder.DropTable(
                name: "Student");

            migrationBuilder.DropTable(
                name: "StudentDegreePlan");
        }
    }
}
