﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyMSACS.Data;
using MyMSACS.Models;

namespace MyMSACS.Controllers
{
    public class PlanTermsController : Controller
    {
        private readonly StudentContext _context;

        public PlanTermsController(StudentContext context)
        {
            _context = context;
        }

        // GET: PlanTerms
        public async Task<IActionResult> Index()
        {
            return View(await _context.PlanTerms.ToListAsync());
        }

        // GET: PlanTerms/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var planTerm = await _context.PlanTerms
                .SingleOrDefaultAsync(m => m.PlanTermId == id);
            if (planTerm == null)
            {
                return NotFound();
            }

            return View(planTerm);
        }

        // GET: PlanTerms/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PlanTerms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlanTermId,StudentId,DegreeId,PlanNumber,TermNumber,TermAbbrev")] PlanTerm planTerm)
        {
            if (ModelState.IsValid)
            {
                _context.Add(planTerm);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(planTerm);
        }

        // GET: PlanTerms/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var planTerm = await _context.PlanTerms.SingleOrDefaultAsync(m => m.PlanTermId == id);
            if (planTerm == null)
            {
                return NotFound();
            }
            return View(planTerm);
        }

        // POST: PlanTerms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PlanTermId,StudentId,DegreeId,PlanNumber,TermNumber,TermAbbrev")] PlanTerm planTerm)
        {
            if (id != planTerm.PlanTermId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(planTerm);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlanTermExists(planTerm.PlanTermId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(planTerm);
        }

        // GET: PlanTerms/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var planTerm = await _context.PlanTerms
                .SingleOrDefaultAsync(m => m.PlanTermId == id);
            if (planTerm == null)
            {
                return NotFound();
            }

            return View(planTerm);
        }

        // POST: PlanTerms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var planTerm = await _context.PlanTerms.SingleOrDefaultAsync(m => m.PlanTermId == id);
            _context.PlanTerms.Remove(planTerm);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlanTermExists(int id)
        {
            return _context.PlanTerms.Any(e => e.PlanTermId == id);
        }
    }
}
