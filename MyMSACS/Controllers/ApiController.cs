﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MyMSACS.Models;
using System.Linq;
using MyMSACS.Data;

namespace TodoApi.Controllers
{
    [Route("api/api")]
    public class TodoController : Controller
    {
        private readonly StudentContext _context;

        public TodoController(StudentContext context)
        {
            _context = context;

            if (_context.Students.Count() == 0)
            {
                _context.Students.Add(new Student {FamilyName="Amarishwer",GivenName="Edam"});
                _context.SaveChanges();
            }
        }
        [HttpGet]
        public IEnumerable<Student> GetAll()
        {
            return _context.Students.ToList();
        }

        [HttpGet("{id}", Name = "GetApi")]
        public IActionResult GetById(long id)
        {
            var item = _context.Students.FirstOrDefault(t => t.StudentId == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
        [HttpPost]
        public IActionResult Create([FromBody] Student item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _context.Students.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetTodo", new { id = item.StudentId }, item);
        }
        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Student item)
        {
            if (item == null || item.StudentId != id)
            {
                return BadRequest();
            }

            var todo = _context.Students.FirstOrDefault(t => t.StudentId == id);
            if (todo == null)
            {
                return NotFound();
            }

            todo.FamilyName = item.FamilyName;
            todo.GivenName = item.GivenName;
            
            

            _context.Students.Update(todo);
            _context.SaveChanges();
            return new NoContentResult();
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var todo = _context.Students.FirstOrDefault(t => t.StudentId == id);
            if (todo == null)
            {
                return NotFound();
            }

            _context.Students.Remove(todo);
            _context.SaveChanges();
            return new NoContentResult();
        }
    }
}