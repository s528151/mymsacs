﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyMSACS.Data;
using MyMSACS.Models;

namespace MyMSACS.Controllers
{
    public class RequirementStatusController : Controller
    {
        private readonly StudentContext _context;

        public RequirementStatusController(StudentContext context)
        {
            _context = context;
        }

        // GET: RequirementStatus
        public async Task<IActionResult> Index()
        {
            return View(await _context.RequirementStatuses.ToListAsync());
        }

        // GET: RequirementStatus/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var requirementStatus = await _context.RequirementStatuses
                .SingleOrDefaultAsync(m => m.RequirementStatusId == id);
            if (requirementStatus == null)
            {
                return NotFound();
            }

            return View(requirementStatus);
        }

        // GET: RequirementStatus/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: RequirementStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("RequirementState,RequirementStatusId")] RequirementStatus requirementStatus)
        {
            if (ModelState.IsValid)
            {
                _context.Add(requirementStatus);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(requirementStatus);
        }

        // GET: RequirementStatus/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var requirementStatus = await _context.RequirementStatuses.SingleOrDefaultAsync(m => m.RequirementStatusId == id);
            if (requirementStatus == null)
            {
                return NotFound();
            }
            return View(requirementStatus);
        }

        // POST: RequirementStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("RequirementState,RequirementStatusId")] RequirementStatus requirementStatus)
        {
            if (id != requirementStatus.RequirementStatusId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(requirementStatus);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RequirementStatusExists(requirementStatus.RequirementStatusId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(requirementStatus);
        }

        // GET: RequirementStatus/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var requirementStatus = await _context.RequirementStatuses
                .SingleOrDefaultAsync(m => m.RequirementStatusId == id);
            if (requirementStatus == null)
            {
                return NotFound();
            }

            return View(requirementStatus);
        }

        // POST: RequirementStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var requirementStatus = await _context.RequirementStatuses.SingleOrDefaultAsync(m => m.RequirementStatusId == id);
            _context.RequirementStatuses.Remove(requirementStatus);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RequirementStatusExists(int id)
        {
            return _context.RequirementStatuses.Any(e => e.RequirementStatusId == id);
        }
    }
}
