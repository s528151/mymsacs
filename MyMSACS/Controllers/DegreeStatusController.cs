﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyMSACS.Data;
using MyMSACS.Models;

namespace MyMSACS.Controllers
{
    public class DegreeStatusController : Controller
    {
        private readonly StudentContext _context;

        public DegreeStatusController(StudentContext context)
        {
            _context = context;
        }

        // GET: DegreeStatus
        public async Task<IActionResult> Index()
        {
            return View(await _context.DegreeStatuses.ToListAsync());
        }

        // GET: DegreeStatus/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var degreeStatus = await _context.DegreeStatuses
                .SingleOrDefaultAsync(m => m.DegreeStatusId == id);
            if (degreeStatus == null)
            {
                return NotFound();
            }

            return View(degreeStatus);
        }

        // GET: DegreeStatus/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DegreeStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DegreeStatusId,DegreeState")] DegreeStatus degreeStatus)
        {
            if (ModelState.IsValid)
            {
                _context.Add(degreeStatus);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(degreeStatus);
        }

        // GET: DegreeStatus/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var degreeStatus = await _context.DegreeStatuses.SingleOrDefaultAsync(m => m.DegreeStatusId == id);
            if (degreeStatus == null)
            {
                return NotFound();
            }
            return View(degreeStatus);
        }

        // POST: DegreeStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("DegreeStatusId,DegreeState")] DegreeStatus degreeStatus)
        {
            if (id != degreeStatus.DegreeStatusId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(degreeStatus);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DegreeStatusExists(degreeStatus.DegreeStatusId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(degreeStatus);
        }

        // GET: DegreeStatus/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var degreeStatus = await _context.DegreeStatuses
                .SingleOrDefaultAsync(m => m.DegreeStatusId == id);
            if (degreeStatus == null)
            {
                return NotFound();
            }

            return View(degreeStatus);
        }

        // POST: DegreeStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var degreeStatus = await _context.DegreeStatuses.SingleOrDefaultAsync(m => m.DegreeStatusId == id);
            _context.DegreeStatuses.Remove(degreeStatus);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DegreeStatusExists(int id)
        {
            return _context.DegreeStatuses.Any(e => e.DegreeStatusId == id);
        }
    }
}
