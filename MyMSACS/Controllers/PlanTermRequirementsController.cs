﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyMSACS.Data;
using MyMSACS.Models;

namespace MyMSACS.Controllers
{
    public class PlanTermRequirementsController : Controller
    {
        private readonly StudentContext _context;

        public PlanTermRequirementsController(StudentContext context)
        {
            _context = context;
        }

        // GET: PlanTermRequirements
        public async Task<IActionResult> Index()
        {
            return View(await _context.TermRequirements.ToListAsync());
        }

        // GET: PlanTermRequirements/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var planTermRequirement = await _context.TermRequirements
                .SingleOrDefaultAsync(m => m.PlanTermRequirementId == id);
            if (planTermRequirement == null)
            {
                return NotFound();
            }

            return View(planTermRequirement);
        }

        // GET: PlanTermRequirements/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PlanTermRequirements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlanTermRequirementId,StudentId,DegreeId,PlanNumber,TermNumber,RequirementNumber")] PlanTermRequirement planTermRequirement)
        {
            if (ModelState.IsValid)
            {
                _context.Add(planTermRequirement);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(planTermRequirement);
        }

        // GET: PlanTermRequirements/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var planTermRequirement = await _context.TermRequirements.SingleOrDefaultAsync(m => m.PlanTermRequirementId == id);
            if (planTermRequirement == null)
            {
                return NotFound();
            }
            return View(planTermRequirement);
        }

        // POST: PlanTermRequirements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PlanTermRequirementId,StudentId,DegreeId,PlanNumber,TermNumber,RequirementNumber")] PlanTermRequirement planTermRequirement)
        {
            if (id != planTermRequirement.PlanTermRequirementId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(planTermRequirement);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlanTermRequirementExists(planTermRequirement.PlanTermRequirementId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(planTermRequirement);
        }

        // GET: PlanTermRequirements/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var planTermRequirement = await _context.TermRequirements
                .SingleOrDefaultAsync(m => m.PlanTermRequirementId == id);
            if (planTermRequirement == null)
            {
                return NotFound();
            }

            return View(planTermRequirement);
        }

        // POST: PlanTermRequirements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var planTermRequirement = await _context.TermRequirements.SingleOrDefaultAsync(m => m.PlanTermRequirementId == id);
            _context.TermRequirements.Remove(planTermRequirement);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlanTermRequirementExists(int id)
        {
            return _context.TermRequirements.Any(e => e.PlanTermRequirementId == id);
        }
    }
}
