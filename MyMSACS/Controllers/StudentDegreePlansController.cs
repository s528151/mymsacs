﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyMSACS.Data;
using MyMSACS.Models;

namespace MyMSACS.Controllers
{
    public class StudentDegreePlansController : Controller
    {
        private readonly StudentContext _context;

        public StudentDegreePlansController(StudentContext context)
        {
            _context = context;
        }

        // GET: StudentDegreePlans
        public async Task<IActionResult> Index()
        {
            return View(await _context.StudentDegreePlans.ToListAsync());
        }

        // GET: StudentDegreePlans/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var studentDegreePlan = await _context.StudentDegreePlans
                .SingleOrDefaultAsync(m => m.StudentDegreePlanId == id);
            if (studentDegreePlan == null)
            {
                return NotFound();
            }

            return View(studentDegreePlan);
        }

        // GET: StudentDegreePlans/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: StudentDegreePlans/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StudentId,StudentDegreePlanId,PlanNumber,PlanAbbrev,PlanName,CreateDate,EditDate,DegreeStatusId,DegreeId,lkDegreeStatus")] StudentDegreePlan studentDegreePlan)
        {
            if (ModelState.IsValid)
            {
                _context.Add(studentDegreePlan);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(studentDegreePlan);
        }

        // GET: StudentDegreePlans/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var studentDegreePlan = await _context.StudentDegreePlans.SingleOrDefaultAsync(m => m.StudentDegreePlanId == id);
            if (studentDegreePlan == null)
            {
                return NotFound();
            }
            return View(studentDegreePlan);
        }

        // POST: StudentDegreePlans/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("StudentId,StudentDegreePlanId,PlanNumber,PlanAbbrev,PlanName,CreateDate,EditDate,DegreeStatusId,DegreeId,lkDegreeStatus")] StudentDegreePlan studentDegreePlan)
        {
            if (id != studentDegreePlan.StudentDegreePlanId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(studentDegreePlan);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentDegreePlanExists(studentDegreePlan.StudentDegreePlanId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(studentDegreePlan);
        }

        // GET: StudentDegreePlans/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var studentDegreePlan = await _context.StudentDegreePlans
                .SingleOrDefaultAsync(m => m.StudentDegreePlanId == id);
            if (studentDegreePlan == null)
            {
                return NotFound();
            }

            return View(studentDegreePlan);
        }

        // POST: StudentDegreePlans/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var studentDegreePlan = await _context.StudentDegreePlans.SingleOrDefaultAsync(m => m.StudentDegreePlanId == id);
            _context.StudentDegreePlans.Remove(studentDegreePlan);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StudentDegreePlanExists(int id)
        {
            return _context.StudentDegreePlans.Any(e => e.StudentDegreePlanId == id);
        }
    }
}
