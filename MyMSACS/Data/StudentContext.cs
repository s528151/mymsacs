﻿using Microsoft.EntityFrameworkCore;
using MyMSACS.Models;

namespace MyMSACS.Data
{
    public class StudentContext: DbContext
    {
        public StudentContext(DbContextOptions<StudentContext> options) : base(options)
        {
        }

        public DbSet<Degree> Degrees { get; set; }
        public DbSet<DegreeRequirement> DegreeRequirements { get; set; }
        public DbSet<PlanTerm> PlanTerms { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<StudentDegreePlan> StudentDegreePlans { get; set; }
        public DbSet<PlanTermRequirement> TermRequirements { get; set; }
        public DbSet<DegreeStatus> DegreeStatuses { get; set; }
        public DbSet<RequirementStatus> RequirementStatuses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Degree>().ToTable("Degree");
            modelBuilder.Entity<DegreeRequirement>().ToTable("DegreeRequirement");
            modelBuilder.Entity<DegreeStatus>().ToTable("DegreeStatus");
            modelBuilder.Entity<RequirementStatus>().ToTable("RequirementStatus");
            modelBuilder.Entity<Student>().ToTable("Student");
            modelBuilder.Entity<PlanTerm>().ToTable("PlanTerm");
            modelBuilder.Entity<StudentDegreePlan>().ToTable("StudentDegreePlan");
            modelBuilder.Entity<PlanTermRequirement>().ToTable("PlanTermRequirement");
        }
    }
}
