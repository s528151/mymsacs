﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyMSACS.Models
{
    public class RequirementStatus
    {
        [Required]
        [StringLength(20, ErrorMessage = "Abbreviation cannot be longer than 20 characters.")]
        public string RequirementState { get; internal set; }
        
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RequirementStatusId { get; internal set; }
    }
}
