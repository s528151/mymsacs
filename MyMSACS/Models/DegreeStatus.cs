﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyMSACS.Models
{
    public class DegreeStatus
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DegreeStatusId { get; internal set; }
        [Required]
        [StringLength(20, ErrorMessage = "Abbreviation cannot be longer than 20 characters.")]
        public string DegreeState { get; internal set; }
    }
}
