﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyMSACS.Models
{
    public class DegreeRequirement
    {

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DegreeRequirementId { get; internal set; }
        public int DegreeId { get; internal set; }
        [Required]
        [Display(Name = "Requirement Number (used for sorting)")]
        public int RequirementNumber { get; internal set; }
        [Required]
        [StringLength(10, ErrorMessage = "Abbreviation cannot be longer than 10 characters.")]
        public string RequirementAbbrev { get; internal set; }
        [Required]
        [StringLength(40, ErrorMessage = "Abbreviation cannot be longer than 40 characters.")]
        public string RequirementName { get; internal set; }
        [Required]
        [StringLength(20, ErrorMessage = "Abbreviation cannot be longer than 20 characters.")]
        public string lkDegree { get; internal set; }
    }
}
