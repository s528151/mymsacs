﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyMSACS.Models
{
    public class PlanTerm
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PlanTermId { get; internal set; }
        public int StudentId { get; internal set; }
        public int DegreeId { get; internal set; }
        [Required]
        [Display(Name = "Plan Number (used for sorting)")]
        public int PlanNumber { get; internal set; }
        [Required]
        [Display(Name = "Term Number (used for sorting)")]
        public int TermNumber { get; internal set; }
        [Required]
        [StringLength(10, ErrorMessage = "Abbreviation cannot be longer than 10 characters.")]
        public string TermAbbrev { get; internal set; }
    }
}
