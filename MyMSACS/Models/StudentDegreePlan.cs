﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyMSACS.Models
{
    public class StudentDegreePlan
    {
         

        public int StudentId { get; internal set; }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StudentDegreePlanId { get; internal set; }
        [Required]
        [Display(Name = "Plan Number (used for sorting)")]
        public int PlanNumber { get; internal set; }
        [Required]
        [StringLength(20, ErrorMessage = "Abbreviation cannot be longer than 20 characters.")]
        public string PlanAbbrev { get; internal set; }
        [Required]
        [StringLength(20, ErrorMessage = "Abbreviation cannot be longer than 20 characters.")]
        public string PlanName { get; internal set; }
        [Required]
        [StringLength(20, ErrorMessage = "Abbreviation cannot be longer than 20 characters.")]
        public String CreateDate { get; internal set; }
        [Required]
        [StringLength(20, ErrorMessage = "Abbreviation cannot be longer than 20 characters.")]
        public String EditDate { get; internal set; }
        [Required]
        public int DegreeStatusId { get; internal set; }
        [Required]
        public int DegreeId { get; internal set; }
        [Required]
        [StringLength(20, ErrorMessage = "Abbreviation cannot be longer than 20 characters.")]
        public string lkDegreeStatus { get; internal set; }
    }
}
