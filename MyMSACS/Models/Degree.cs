﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace MyMSACS.Models
{
    public class Degree
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DegreeId { get; internal set; }
        [Required]
        [StringLength(10, ErrorMessage = "Abbreviation cannot be longer than 10 characters.")]
        public string DegreeAbbrev { get; internal set; }
        [Required]
        [StringLength(100, ErrorMessage = "Abbreviation cannot be longer than 100 characters.")]
        public string DegreeName { get; internal set; }
    }
}
